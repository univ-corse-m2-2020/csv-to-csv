﻿using DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesContracts
{
    public abstract class AbstractFunctoid
    {
        public abstract List<FakeBlock> CreateBlock(Int32 nbOfOutputs, Dictionary<String,String> parametes, List<FakeBlock> entries);
    }
}
